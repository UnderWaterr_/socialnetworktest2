package itis.socialtest;


import itis.socialtest.entities.Author;
import itis.socialtest.entities.Post;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Long.parseLong;


/*
 * В папке resources находятся два .csv файла.
 * Один содержит данные о постах в соцсети в следующем формате: Id автора, число лайков, дата, текст
 * Второй содержит данные о пользователях  - id, никнейм и дату рождения
 *
 * Напишите код, который превратит содержимое файлов в обьекты в package "entities"
 * и осуществите над ними следующие опреации:
 *
 * 1. Выведите в консоль все посты в читабельном виде, с информацией об авторе.
 * 2. Выведите все посты за сегодняшнюю дату
 * 3. Выведите все посты автора с ником "varlamov"
 * 4. Проверьте, содержит ли текст хотя бы одного поста слово "Россия"
 * 5. Выведите никнейм самого популярного автора (определять по сумме лайков на всех постах)
 *
 * Для выполнения заданий 2-5 используйте методы класса AnalyticsServiceImpl (которые вам нужно реализовать).
 *
 * Требования к реализации: все методы в AnalyticsService должны быть реализованы с использованием StreamApi.
 * Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
 * Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
 *
 *
 * */

public class MainClass {

    private List<Post> allPosts;

    private AnalyticsService analyticsService = new AnalyticsServiceImpl();

    public static void main(String[] args) {
        ArrayList<Post> posts = new MainClass().run("C:\\Users\\79196\\IdeaProjects\\socialnetworktest2\\src\\itis\\socialtest\\resources\\PostDatabase.csv",
                "C:\\Users\\79196\\IdeaProjects\\socialnetworktest2\\src\\itis\\socialtest\\resources\\Authors.csv");
        assert posts != null;
        System.out.println(posts.toString());
        AnalyticsService analyticsService = new AnalyticsServiceImpl();
        System.out.println(analyticsService.findPostsByDate(posts, "17.04.2021T10:00"));
        System.out.println(analyticsService.findMostPopularAuthorNickname(posts));

        System.out.println(analyticsService.findAllPostsByAuthorNickname(posts, "varlamov"));
        System.out.println(analyticsService.checkPostsThatContainsSearchString(posts, "России"));
    }

    private ArrayList<Post> run(String postsSourcePath, String authorsSourcePath) {
        try {
            File authorsSource = new File(authorsSourcePath);
            File postsSource = new File(postsSourcePath);

            FileReader postsFileReader = new FileReader(postsSource);
            FileReader authorsFileReader = new FileReader(authorsSource);

            BufferedReader authorsBufferedReader = new BufferedReader(authorsFileReader);

            ArrayList<Author> authors = new ArrayList<>();
            ArrayList<Post> posts = new ArrayList<>();

            String author;
            String post;
            while (true) {
                author = authorsBufferedReader.readLine();

                if (author == null) {
                    break;
                }
                String[] stringAuthor = author.split(", ");

                authors.add(new Author(Long.parseLong(stringAuthor[0]), stringAuthor[1], stringAuthor[2]));
            }

            authorsBufferedReader.close();
            BufferedReader postsBufferedReader = new BufferedReader(postsFileReader);
            while (true) {
                post = postsBufferedReader.readLine();
                if (post == null) {
                    break;
                }
                String[] stringPost = post.split(", ");
                for (int i = 4; i < stringPost.length; i++) {
                    stringPost[3]+= ", " + stringPost[i];
                }
                posts.add(new Post(stringPost[2], stringPost[3], Long.parseLong(stringPost[1]),
                        findAuthor(authors, Long.parseLong(stringPost[0]))));
            }
            return posts;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Author findAuthor(ArrayList<Author> authors, Long id) {
        return authors.stream().filter(author -> author.getId().equals(id)).findFirst().orElse(null);
    }
}